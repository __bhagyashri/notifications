const app = require('express')();
const http = require('http').Server(app);
const path = require('path');
const express = require('express');
const io = require('socket.io')(http);
var date = Date.now();
const messages = [{
		id: 1,
	  	type: 'Tasks',
	  	text: 'Some Task',
	  	arrivalTime: Date.now(),
	  	closeIcon: '',
	  	linkUrl: '',
	  	linkText: 'View Task'
	  },
	  {
	  	id: 2,
	  	type: 'notification',
	  	text: 'Some notification',
	  	arrivalTime: Date.now(),
	  	closeIcon: '',
	  	linkUrl: '',
	  	linkText: 'View Pipeline'
	  },
	  {
	  	id: 3,
	  	type: 'reminder',
	  	text: 'Some Reminder',
	  	arrivalTime: Date.now(),
	  	closeIcon: '',
	  	linkUrl: '',
	  	linkText: 'View offer'
	  }];

app.use(express.static(path.join(__dirname, 'dist')));

// Rendering index.html
app.get('/', (req, res) => {
  res.sendFile(path.join(__dirname, 'dist/index.html'));
});

//socket connection and events
io.on('connection', (socket) => {
  	console.log('a user connected');
  	
  	var count = 0;
  	var sendMessage = setInterval(() => {
  	var randomMessage = messages[count];

  	io.emit('message', randomMessage);
  	if(count < (messages.length-1)) {
  		count++;
  		//console.log('increased ' + count);
  	} else {
  		count--;
  		//console.log('decresed ' + count);
  	}
  }, 5000);

  socket.on('disconnect', () => {
    console.log('user disconnected');
    clearInterval(sendMessage);
  });

});

// Starting http server 
http.listen(3000, () => {
	console.log('listening on *: 3000');
});