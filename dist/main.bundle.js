webpackJsonp([1],{

/***/ "../../../../../src async recursive":
/***/ (function(module, exports) {

function webpackEmptyContext(req) {
	throw new Error("Cannot find module '" + req + "'.");
}
webpackEmptyContext.keys = function() { return []; };
webpackEmptyContext.resolve = webpackEmptyContext;
module.exports = webpackEmptyContext;
webpackEmptyContext.id = "../../../../../src async recursive";

/***/ }),

/***/ "../../../../../src/app/app.component.html":
/***/ (function(module, exports) {

module.exports = "<!--The content below is only a placeholder and can be replaced.-->\n<div class=\"navbar\">\n  <span (click)='displayNotifications = !displayNotifications'>\n  <span *ngIf='noOfnotifications > 0' class=\"noOfnotifications\">{{noOfnotifications}}</span>\n    <i class=\"material-icons\">notifications</i>\n  </span>\n\n  <h3>    {{userName}}  </h3>\n\n  <div class=\"notificationsContainer\" *ngIf=\"displayNotifications\">\n      \n      <p class='date'>{{todaysDate | date:'EEEE'}} {{todaysDate | date:'longDate'}}</p>     \n      <div class=\"notificationSummary\">\n            <p> \n                <span class=\"summaryCounts\">{{assignedTasksCount}} </span> \n                <span class=\"countLabels\"> Assigned Tasks </span> \n            </p>\n            <p> \n                <span class=\"summaryCounts\">{{remindersCount}} </span> \n                <span class=\"countLabels\"> Reminders </span> \n            </p>\n            <p>\n                <span class=\"summaryCounts\"> {{ notificationsCount}}</span> \n                <span class=\"countLabels\"> Notifications </span> \n            </p>\n      </div>\n      <p class=\"workspaceLink\"><a href=\"#\"> Got to My Workspace &#62;</a></p>     \n      <ul class=\"notificationsList\">\n        <li *ngFor=\"let notification of notificationsList\">\n            <h2>{{notification.arrivalTime}}</h2>\n            <!-- <h2>2 seconds</h2> -->\n            <span><i class=\"material-icons\">close</i></span>\n            <p>{{notification.text}} <a href=\"#\"> {{notification.linkText}} &#62;</a></p>\n            <hr>\n        </li>\n      </ul>\n  </div>\n</div>\n\n\n"

/***/ }),

/***/ "../../../../../src/app/app.component.scss":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, ".navbar {\n  height: 50px;\n  background-color: #000000;\n  color: #ffffff;\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-pack: end;\n      -ms-flex-pack: end;\n          justify-content: flex-end;\n  -webkit-box-align: center;\n      -ms-flex-align: center;\n          align-items: center;\n  padding: 10px 20px; }\n\n.navbar span {\n  width: 30px;\n  margin: 0px 10px;\n  cursor: pointer; }\n\n.noOfnotifications {\n  font-size: 10px;\n  color: red;\n  position: absolute;\n  right: 135px; }\n\n.navbar > .notificationsContainer {\n  position: absolute;\n  top: 70px;\n  right: 0px;\n  color: black;\n  border: none;\n  border-top: 10px solid #CCCCCC;\n  width: 375px;\n  padding: 0px;\n  background: #ffffff; }\n\n.notificationsContainer span {\n  margin: 0px; }\n\n.date {\n  font-family: Myrid Set Pro, Sans-serif;\n  font-size: 28px;\n  color: #333;\n  font-weight: 100;\n  border: none;\n  border-bottom: 1px solid #D5D5D5;\n  padding: 10px 26px; }\n\n.notificationSummary {\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  padding-top: 45px;\n  padding-bottom: 26px;\n  padding: 0px 26px; }\n\n.notificationSummary p {\n  padding: 10px;\n  text-align: center; }\n\n.notificationSummary p:first-child {\n  padding-left: 0px;\n  padding-right: 5px; }\n\n.notificationSummary p:last-child {\n  padding-right: 0px; }\n\n.notificationSummary .summaryCounts {\n  font-family: Myrid Set Pro Ultralight, Sans-serif;\n  font-size: 80px;\n  color: #47ACF5;\n  font-weight: lighter; }\n\n.notificationSummary .countLabels {\n  font-family: Myrid Set Pro Text, Sans-serif;\n  font-size: 16px;\n  color: #999;\n  padding-top: 18px;\n  line-height: 23px; }\n\n.workspaceLink {\n  background-color: #F7F7F7;\n  height: 32px;\n  text-align: center;\n  padding: 0px 26px;\n  padding-top: 12px; }\n\n.workspaceLink a {\n  font-family: Myrid Set Pro Text, Sans-serif;\n  font-size: 14px;\n  color: #4C9DD6; }\n\n.notificationsList {\n  padding: 20px 0px 20px 26px; }\n\n.notificationsList li h2 {\n  font-family: Myrid Set Pro Medium, Sans-serif;\n  font-size: 14px;\n  color: #333;\n  line-height: 18px;\n  text-transform: capitalize;\n  display: inline; }\n\n.notificationsList li span i {\n  position: relative;\n  top: 5px;\n  left: 200px;\n  font-size: 20px;\n  color: #ccc; }\n\n.notificationsList li p {\n  font-family: Myrid Set Pro Medium, Sans-serif;\n  font-size: 14px;\n  color: #757575;\n  padding: 10px 26px 10px 0px; }\n  .notificationsList li p a {\n    font-family: Myrid Set Pro Medium, Sans-serif;\n    font-size: 14px;\n    color: #4C9DD6; }\n\n.notificationsList li hr {\n  border: 1px solid #ccc; }\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/app.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__app_service__ = __webpack_require__("../../../../../src/app/app.service.ts");
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var AppComponent = (function () {
    function AppComponent(appService) {
        this.appService = appService;
        this.todaysDate = new Date();
        this.userName = 'Johnny Applessed';
        this.displayNotifications = false;
        this.assignedTasksCount = 0;
        this.remindersCount = 0;
        this.notificationsCount = 0;
        this.notificationsList = [];
        this.noOfnotifications = this.notificationsList.length;
    }
    AppComponent.prototype.calucateArrivalTime = function (arrivalTime) {
        var currentDate = Date.now();
        var timeDifference = '';
        var difference = Math.floor((currentDate - arrivalTime) / 1000);
        if (difference < 60) {
            timeDifference = (difference > 1) ? difference + ' Seconds Ago' : 'Just now';
        }
        else if (difference > 60 && difference < 3600) {
            var minutes = this.countMinutes(difference);
            timeDifference = (minutes > 1) ? minutes + ' Minutes Ago' : minutes + ' Minute Ago';
        }
        else if (difference > 3600) {
            var hours = this.countHours(difference);
            timeDifference = (hours > 1) ? hours + ' Hours Ago' : hours + ' Hour Ago';
        }
        else {
            var days = this.countDays(difference);
            timeDifference = (days > 1) ? days + ' Days Ago' : days + ' Day Ago';
        }
        console.log(difference);
        return timeDifference;
    };
    AppComponent.prototype.countMinutes = function (seconds) {
        return Math.round(seconds / 60);
    };
    AppComponent.prototype.countHours = function (seconds) {
        return Math.round(seconds / 3600);
    };
    AppComponent.prototype.countDays = function (seconds) {
        return Math.round(seconds / (3600 * 24));
    };
    AppComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.connection = this.appService.getMessages().subscribe(function (message) {
            console.log(message);
            /*var notification:any = {id: message.id,
                                    type: message.type,
                                    text: message.text,
                                    arrivalTime: message.arrivalTime,
                                    closeIcon: '<i class="material-icons">close</i>',
                                    linkUrl: '',
                                    linkText: message.linkText}; */
            var notification = message;
            notification.arrivalTime = _this.calucateArrivalTime(notification.arrivalTime);
            console.log(notification);
            if (notification.type == 'Tasks') {
                _this.assignedTasksCount++;
            }
            if (notification.type == 'notification') {
                _this.notificationsCount++;
            }
            if (notification.type == 'reminder') {
                _this.remindersCount++;
            }
            _this.notificationsList.push(message);
            _this.noOfnotifications = _this.notificationsList.length;
            //console.log(this.notificationsList);
        });
    };
    AppComponent.prototype.ngOnDestroy = function () {
        this.connection.unsubscribe();
    };
    return AppComponent;
}());
AppComponent = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_0" /* Component */])({
        selector: 'app-root',
        template: __webpack_require__("../../../../../src/app/app.component.html"),
        styles: [__webpack_require__("../../../../../src/app/app.component.scss")],
        providers: [__WEBPACK_IMPORTED_MODULE_1__app_service__["a" /* AppService */]]
    }),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__app_service__["a" /* AppService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__app_service__["a" /* AppService */]) === "function" && _a || Object])
], AppComponent);

var _a;
//# sourceMappingURL=app.component.js.map

/***/ }),

/***/ "../../../../../src/app/app.module.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__ = __webpack_require__("../../../platform-browser/@angular/platform-browser.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__app_component__ = __webpack_require__("../../../../../src/app/app.component.ts");
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppModule; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var AppModule = (function () {
    function AppModule() {
    }
    return AppModule;
}());
AppModule = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_1__angular_core__["b" /* NgModule */])({
        declarations: [
            __WEBPACK_IMPORTED_MODULE_2__app_component__["a" /* AppComponent */]
        ],
        imports: [
            __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__["a" /* BrowserModule */]
        ],
        providers: [],
        bootstrap: [__WEBPACK_IMPORTED_MODULE_2__app_component__["a" /* AppComponent */]]
    })
], AppModule);

//# sourceMappingURL=app.module.js.map

/***/ }),

/***/ "../../../../../src/app/app.service.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_rxjs_Observable__ = __webpack_require__("../../../../rxjs/Observable.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_rxjs_Observable___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_rxjs_Observable__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_socket_io_client__ = __webpack_require__("../../../../socket.io-client/lib/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_socket_io_client___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_socket_io_client__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppService; });


var AppService = (function () {
    function AppService() {
        this.url = 'http://localhost:3000';
    }
    AppService.prototype.getMessages = function () {
        var _this = this;
        var observable = new __WEBPACK_IMPORTED_MODULE_0_rxjs_Observable__["Observable"](function (observer) {
            _this.socket = __WEBPACK_IMPORTED_MODULE_1_socket_io_client__(_this.url);
            _this.socket.on('message', function (data) {
                observer.next(data);
            });
            return function () {
                _this.socket.disconnect();
            };
        });
        return observable;
    };
    return AppService;
}());

//# sourceMappingURL=app.service.js.map

/***/ }),

/***/ "../../../../../src/environments/environment.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return environment; });
// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.
// The file contents for the current environment will overwrite these during build.
var environment = {
    production: false
};
//# sourceMappingURL=environment.js.map

/***/ }),

/***/ "../../../../../src/main.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_platform_browser_dynamic__ = __webpack_require__("../../../platform-browser-dynamic/@angular/platform-browser-dynamic.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__app_app_module__ = __webpack_require__("../../../../../src/app/app.module.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__environments_environment__ = __webpack_require__("../../../../../src/environments/environment.ts");




if (__WEBPACK_IMPORTED_MODULE_3__environments_environment__["a" /* environment */].production) {
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["a" /* enableProdMode */])();
}
__webpack_require__.i(__WEBPACK_IMPORTED_MODULE_1__angular_platform_browser_dynamic__["a" /* platformBrowserDynamic */])().bootstrapModule(__WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */]);
//# sourceMappingURL=main.js.map

/***/ }),

/***/ 0:
/***/ (function(module, exports) {

/* (ignored) */

/***/ }),

/***/ 1:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__("../../../../../src/main.ts");


/***/ })

},[1]);
//# sourceMappingURL=main.bundle.js.map