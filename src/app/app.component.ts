import { Component, OnInit, OnDestroy } from '@angular/core';
import { AppService } from './app.service'
 
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
  providers: [AppService] 
})
export class AppComponent implements OnInit, OnDestroy{
	todaysDate:Date = new Date();
	userName:String = 'Johnny Applessed';
	displayNotifications = false;
	assignedTasksCount = 0;
	remindersCount = 0;
	notificationsCount = 0;
	notificationsList:any[] = [];
	noOfnotifications = this.notificationsList.length;
	connection;

	constructor(private appService:AppService) {}

	calucateArrivalTime(arrivalTime) {
		var currentDate = Date.now();
		var timeDifference = '';
		var difference = Math.floor((currentDate - arrivalTime)/1000);

		if(difference < 60) {
			timeDifference = (difference > 1) ? difference + ' Seconds Ago' : 'Just now';
		} else if(difference > 60 && difference < 3600) {
			var minutes = this.countMinutes(difference);
			timeDifference = (minutes > 1) ? minutes + ' Minutes Ago' :  minutes + ' Minute Ago';
		} else if(difference > 3600) {
			var hours = this.countHours(difference);
			timeDifference = (hours > 1) ? hours + ' Hours Ago' : hours + ' Hour Ago';
		} else {
			var days = this.countDays(difference);
			timeDifference = (days> 1) ? days + ' Days Ago' : days + ' Day Ago';
		}
		console.log(difference);
		return timeDifference;
	}

	countMinutes(seconds) {
		return Math.round(seconds/60);
	}

	countHours(seconds) {
		return Math.round(seconds/3600);
	}

	countDays(seconds) {
		return Math.round(seconds/(3600*24));
	}

  	ngOnInit() {
    	this.connection = this.appService.getMessages().subscribe(message => {
    		console.log(message);
    		/*var notification:any = {id: message.id,
								  	type: message.type,
								  	text: message.text,
								  	arrivalTime: message.arrivalTime,
								  	closeIcon: '<i class="material-icons">close</i>',
								  	linkUrl: '',
								  	linkText: message.linkText}; */
		var notification:any = message;
		notification.arrivalTime = this.calucateArrivalTime(notification.arrivalTime);
    	console.log(notification);
    	if(notification.type == 'Tasks') {
    		this.assignedTasksCount++;
    	}
    	if(notification.type == 'notification') {
    		this.notificationsCount++;
    	}
    	if(notification.type == 'reminder') {
    		this.remindersCount++;
    	}

      	this.notificationsList.push(message);
      	this.noOfnotifications = this.notificationsList.length;
      	//console.log(this.notificationsList);
    	});
	}
	
	ngOnDestroy() {
	    this.connection.unsubscribe();
	}

}
