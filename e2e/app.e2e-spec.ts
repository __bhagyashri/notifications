import { NotificationsPage } from './app.po';

describe('notifications App', () => {
  let page: NotificationsPage;

  beforeEach(() => {
    page = new NotificationsPage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!!');
  });
});
