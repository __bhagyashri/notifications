# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* This repository demonstates reusable Notifactions module built in Angular 4

### How do I get set up? ###

* To download this project run git clone https://__bhagyashri@bitbucket.org/__bhagyashri/notifications.git
* Run `npm install` to build the project.
* Run 'node server.js` to run the server.
* Open 'localhost:3000' to check the notifications module

